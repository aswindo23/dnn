import pandas as pd
import numpy as np
import math

class NeuralNetworkArch(object):

    x_size = 0
    h_size = []
    hl_total = 0
    y_size = 0

    def __init__(self, x_size, h_size , hl_total, y_size, enc_size, dec_size):
        self.x_size = x_size
        self.h_size = h_size
        self.hl_total = hl_total
        self.y_size = y_size
        self.enc_size = enc_size
        self.dec_size = dec_size

    def get_xsize(self):
        return self.x_size

    def set_xsize(self, x_size):
        self.x_size = x_size

    def set_weight(self):
        """
        Catatan
        Jika nanti biasnya juga dikalikan dengan bobot naka self.x_size di tambah 1
        """
        weight_matrix = []
        for i in range(0,self.hl_total ):
            if i == 0 :
                #print (("kondisi 1  i : %d") % (i))
                weight_matrix.append(np.random.rand(self.h_size[0] , self.x_size) * self.random_function(self.h_size[0], self.x_size) )
            elif i == self.hl_total  :
                #print (("kondisi 2  i : %d") % (i))
                weight_matrix.append(np.random.rand(self.y_size ,self.h_size[i-1] ) * self.random_function(self.h_size[i-1], self.y_size) )
            else:
                #print (("kondisi 3  i : %d") % (i))
                weight_matrix.append(np.random.rand(self.h_size[i],self.h_size[i-1]) * self.random_function(self.h_size[i-1],self.h_size[i]) )
        return weight_matrix

    def set_derivative_weight(self):
        """
        Catatan
        Jika nanti biasnya juga dikalikan dengan bobot naka self.x_size di tambah 1
        """
        weight_matrix = []
        for i in range(0,self.hl_total ):
            if i == 0 :
                #print (("kondisi 1  i : %d") % (i))
                weight_matrix.append(np.zeros((self.h_size[0] , self.x_size)))
            elif i == self.hl_total  :
                #print (("kondisi 2  i : %d") % (i))
                weight_matrix.append(np.zeros((self.y_size ,self.h_size[i-1] )))
            else:
                #print (("kondisi 3  i : %d") % (i))
                weight_matrix.append(np.zeros((self.h_size[i],self.h_size[i-1])))
        return weight_matrix

    def random_function(self, n1, n2):
        return  math.sqrt( 6 / (n1 + n2))


    def set_hiddenlayer(self):
        hidden_layer = []
        for i in range(0,self.hl_total):
            hidden_layer.append(np.zeros((self.h_size[i])))
        return hidden_layer

    def set_ench(self): # encoder hidden layer
        hidden_layer = []
        for i in range(0,len(self.enc_size)):
            hidden_layer.append(np.zeros((self.enc_size[i])))
        return hidden_layer

    def set_dech(self): # encoder hidden layer
        hidden_layer = []
        for i in range(0,len(self.dec_size)-1):
            hidden_layer.append(np.zeros((self.dec_size[i])))
        return hidden_layer

    def set_output(self):
        return np.zeros((self.y_size))

    def set_bias(self):
        """
        Catatan
        Jika nanti biasnya juga dikalikan dengan bobot naka self.x_size di tambah 1
        """
        bias = []

        for i in range(0,len(self.h_size)+1):
            if i == 0 :
                #print (("kondisi 1  i : %d") % (i))
                bias.append(np.random.rand( self.h_size[0], 1 ))
            elif i == len(self.h_size) :
                #print (("kondisi 2  i : %d") % (i))
                bias.append(np.random.rand(self.y_size ,1  ))
            else:
                #print (("kondisi 3  i : %d") % (i))
                bias.append(np.random.rand( self.h_size[i],1  ))
        return bias

    def set_derivatives_bias(self):
        """
        Catatan
        Jika nanti biasnya juga dikalikan dengan bobot naka self.x_size di tambah 1
        """
        bias = []
        for i in range(0, len(self.h_size)+ 1):
            if i == 0 :
                #print (("kondisi 1  i : %d") % (i))
                bias.append( np.zeros((self.h_size[0],1)) )
            elif i == len(self.h_size)  :
                #print (("kondisi 2  i : %d") % (i))
                bias.append( np.zeros( (self.y_size ,1 )) )
            else:
                #print (("kondisi 3  i : %d") % (i))
                bias.append(np.zeros( (self.h_size[i],1 )) )
        return bias
