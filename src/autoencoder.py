import pandas as pd
import numpy as np
import math

def sigmoid_function(z):
    """
    fungsi sigmoid logistik 1 / 1 + exp(-x)
    :z merupakan kalkulasi network antara w,b dengan formulasi
    sigma W.x + b ; dimana x adalah nilai input dari input layer
    """
    return (1/(1+ math.exp(-z)))

def sigmoid_derivative(activation):
    """
    fungsi derivative dari fungsi logistik sigmoid
    :z merupakan kalkulasi dari jumlah network antara layer ke layer
    """
    return activation * (1-activation)

def create_network( Weight, input, bias, n_hidden, n_input):
    """ Buat komputasi untuk menghitung jumlah network """

    network = np.zeros((n_hidden))
    sum = 0.0

    """
    print (("print n_input : %d ")%(n_input))
    print (("print n_hidden : %d ")%(n_hidden))
    print (("print n_bias : %d ")%(len(bias)))
    print ("-------------------------------------")
    """
    for i in range(0,n_hidden):

        for j in range(0,n_input + 1):
            if j==n_input :
                sum +=  bias[i]
            else:
                sum += Weight[i][j] * input[j]

        network[i] = sigmoid_function(sum)
        sum = 0.0
    return network


def hypotheses(z):
    """
    hypotheses merupakan fungsi untuk aktifasi pada layer output dimana
    hypotheses Hw,b^(x) = a dan a merupakan hasil kalkulasi dari perhitungan
    fungsi aktifasi sigmoid
    :z merupakan kalkulasi dari jumlah network antara layer ke layer
    """
    return sigmoid_function(z)

def weights_transpose(weight):

    weight_transpose = []
    for l in range(len(weight)-1,-1, -1):
        W = weight[l]
        weight_transpose.append( W.transpose((1,0)) )
    return weight_transpose

def cost_function(input_x,hypothesis_x):
    """
    cost function merupakan fungsi biaya yang mengkalkulasikan
    antara output target dengan output aktual dimana output actual adalah y
    dan output target dalah fungsi hypothesis
    :z merupakan kalkulasi dari jumlah network antara layer ke layer
    :y merupakan output aktual
    """
    return 0.5 * math.pow(abs(hypothesis_x - input_x ) , 2)

def count_weight_decay(weights, wdp):
    sum = 0
    for l in range(0,len(weights)):
        layer_weight = weights[l]
        for i in range(0,len(layer_weight[0])):
            for j in range(0,len(layer_weight)):
                sum += math.pow(layer_weight[j][i], 2)
    return (wdp*sum)/2

def overall_cost_function(m_cost_function, weight_decay, control_sparm, KL):
    sum = 0
    for i in range(0,len(m_cost_function)):
        sum += m_cost_function[i]
    return (sum/len(m_cost_function)) + (weight_decay) + (control_sparm * KL)


def sparsity_constrain( recontruction_x, hidden_decoder, hidden_encoder, layer_size):

    sparsity = np.zeros((layer_size,1))
    sum = 0

    for l in range(0,layer_size):
        if l == layer_size :
            for i in range(0,len(recontruction_x)):
                sum += recontruction_x[i]
            sparsity[l] = sum/len(recontruction_x)
            sum = 0
        elif l <= len(hidden_encoder) :
            for i in range(0,len(hidden_encoder)):
                hidden = hidden_encoder[i]
                for j in range(0,len(hidden_encoder[i])):
                    sum += hidden[j]
                sparsity[l] = sum/len(hidden)
                sum = 0
        else:
            for i in range(0,len(hidden_decoder)):
                hidden = hidden_decoder[i]
                for j in range(0, len(hidden_decoder[i])):
                    sum += hidden[j]
                sparsity[l] = sum/len(hidden)
                sum = 0
    return sparsity

def KullbackLeibler(sparsity, s_param):

    sum = 0
    for j in range(0,len(sparsity)):
        sum += (s_param * math.log(s_param/sparsity[j]) ) + ( (1-s_param)* math.log((1-s_param)/(1-sparsity[j])))
    return sum

""" ===================== * BACKWARD PASS * ========================== """

def derivative_error(output, target):
    return -(target - output) * sigmoid_derivative(output)

def derivative_hidden_layer(dE, weight):
    return weight * dE

def derivative_sparsity_contrain(sparam, sparsity, c_sparam):

    return c_sparam * ((-sparam / sparsity) + ((1-sparam) / (1-sparsity)))

def compute_dE(output, target):
    dE = np.zeros((len(output),1))
    for i in range(0,len(output)):
        dE[i] = derivative_error(output[i],target[i])
    return dE

def compute_dH(weights, delta, hidden_layer , sparam, sparsity, c_sparam):
    sum = 0
    dH = []

    for l in range(len(weights),1, -1):
        W = weights[l-1]
        hidden = hidden_layer[l-2]

        if l != len(weights) :
            _tmp2dH = _tmpdH
        _tmpdH = np.zeros((len(W[0]),1))
        for i in range(0,len(W[0])):
            for j in range(0,len(W)):
                if l == len(weights) :
                    sum += derivative_hidden_layer(delta[j] , W[j][i] )
                elif l != len(weights) :
                    sum += derivative_hidden_layer( _tmp2dH[j] , W[j][i] )
            _tmpdH[i] = (sum * derivative_sparsity_contrain(sparam, sparsity[l+1], c_sparam ) )  *  sigmoid_derivative(hidden[i])
            sum = 0
        dH.append(_tmpdH)
    return dH


def compute_encdH(weights, wt, delta, hidden_layer , sparam, sparsity, c_sparam):
    sum = 0
    dH = []


    for l in range(len(weights),0, -1):
        if l == len(weights):
            W = wt[0]
        else :
            W = weights[l]

        hidden = hidden_layer[l-1]

        if l != len(weights) :
            _tmp2dH = _tmpdH
        _tmpdH = np.zeros((len(W[0]),1))
        for i in range(0,len(W[0])):
            for j in range(0,len(W)):
                if l == len(weights) :
                    sum += derivative_hidden_layer(delta[j] , W[j][i] )
                elif l != len(weights) :
                    sum += derivative_hidden_layer( _tmp2dH[j] , W[j][i] )
            _tmpdH[i] = ( sum *  derivative_sparsity_contrain(sparam, sparsity[l-1], c_sparam ) ) * sigmoid_derivative(hidden[i])
            sum = 0
        dH.append(_tmpdH)
    return dH



def compute_weight_derivative(dH, _dW, dE, input, hidden_layer, wdp, weights):

    for l in range(0,len(_dW)):
        dW =  _dW[l]

        _tmpdH = dH[len(_dW) - (l+1) ]

        hidden = hidden_layer[l-1]
        for i in range(0,len(dW[0])):
            m = len(dW[0])
            for j in range(0,len(dW)):
                if l == 0 :
                    _dW[l][j][i] = (( _tmpdH[j] * input[i] ) / m ) + ( wdp * weights[l][j][i] )
                elif l != len(_dW) -1 :
                    _dW[l][j][i] = ((_tmpdH[j] * hidden[i] )/ m )+  ( wdp * weights[l][j][i] )
                elif l == len(_dW) -1 :
                    _dW[l][j][i] = (( dE[j] * hidden[i] )/ m )  + ( wdp * weights[l][j][i] )
    return _dW


def compute_bias_derivative(dH_enc, dH_dec, _dB, dE):

    for l in range(0,len(_dB)):
        dB =  _dB[l]
        if l < len(dH_enc):
            _tmpdH = dH_enc[len(dH_enc) - (l+1)]
        else:
            _tmpdH = dH_dec[len(dH_enc) - (l-1)]


        for i in range(0,len(dB[0])):
            m = len(dB[0])
            for j in range(0,len(dB)):
                if l == 0 :
                    _dB[l][j][i] = (( _tmpdH[j]  ) / m )
                elif l != len(_dB) -1  :
                    _dB[l][j][i] = ((_tmpdH[j]  )/ m )
                elif l == len(_dB) -1 :
                    _dB[l][j][i] = (( dE[j]  )/ m )
    return _dB

def update_weight(dW, weights, learning_rate):

    W = weights

    for l in range(0,len(W)):
        _tmpWeights = W[l]
        for i in range(0,len(_tmpWeights)):
            for j in range(0,len(_tmpWeights[0])):
                W[l][i][j] = W[l][i][j] - (learning_rate * dW[l][i][j])
    return W

def update_bias(dB, bias, learning_rate):

    B = bias
    for l in range(0,len(B)):
        _tmpBias = B[l]
        for i in range(0,len(_tmpBias)):
            for j in range(0,len(_tmpBias[0])):
                B[l][i][j] = B[l][i][j] - (learning_rate * dB[l][i][j])
    return B
