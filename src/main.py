import backpropagation as bp
import normalisasi as norm
from build_network import NeuralNetworkArch
import pandas as pd
import numpy as np
import math
import os
import time


def load_data(file):
    f = open(file,"r", encoding="utf8")
    data = f.readlines()
    f.close()
    return data

def copy_data_matrix(data):

    data_txt = data
    data_mtx = []

    for line in data_txt:
        data_col = line.split('\n')
        tmp_value = data_col[0]
        data_mtx.append(float(tmp_value))

    return data_mtx

def forward_pass(input, weights, hidden_layer, bias,  h_size):

    for l in range(0, len(h_size) + 1 ):
        if l == 0 :
            #print ((" Hidden Layer | %d ") % (l))
            hidden_layer[l] = bp.create_network(weights[l], input, bias[l], h_size[l], len(input))
        elif l != len(h_size) :
            #print ((" Hidden Layer | %d ") % (l))
            hidden_layer[l] = bp.create_network(weights[l], hidden_layer[l-1], bias[l], h_size[l], len(hidden_layer[l-1]))
    return hidden_layer

def forward_pass_output(input, weights,  bias,  output, h_size):

    for l in range(0, len(h_size) + 1 ):
        if l == len(h_size) :
            #print ((" Output Layer | %d ") % (l))
            output = bp.create_network(weights[l], input, bias[l], len(output), len(input))

    return output

def compute_cost_function(output, y_target):
    sum = 0
    error_cf = []
    for i in range(0,len(output)):
         error_cf.append(bp.cost_function(output[i], y_target[i]))

    return error_cf

def load_all_data(file):

    path = []
    f = open(file, "r", encoding="utf8")
    dataset = f.readlines()
    for i in dataset:
        data_col = i.split('\n')
        path.append(data_col[0])
    f.close()

    all_path = []
    for line in range(0,len(path)):
        for root, dirs, files in os.walk(path[line]):
            for filename in files:
                if filename[0:5] == "_NORM" :
                    all_path.append(path[line] + filename)

    return all_path


def main():

    """
    :wdp = weight decay parameter
    """
    x_size = 54675
    h_size = [500, 500, 300]
    hl_total = len(h_size)
    y_size = x_size
    wdp = 0.0005
    learning_rate = 0.001

    print ("Build Network .....")
    dnn = NeuralNetworkArch(x_size, h_size, hl_total, y_size)
    weights = dnn.set_weight()
    output_target = []
    bias = dnn.set_bias()

    print ("Forward Pass ......")

    # registrasi variable
    input = []
    hidden_layer = []
    output = []

    # Load Semua file disini
    path = load_all_data("katalog_dataset.txt")


    for i in range(0,len(path)):

        #"""
        start = time.time()
        hidden = dnn.set_hiddenlayer()
        _dW = dnn.set_derivative_weight()
        _dB = dnn.set_derivatives_bias()
        input.append(copy_data_matrix(load_data(path[i])))
        output_target = copy_data_matrix(load_data(path[i]))
        hidden_layer.append(forward_pass(input[i], weights, hidden  , bias,   h_size))
        hidden = hidden_layer[i]
        output.append(forward_pass_output(hidden[len(h_size) - 1 ], weights,  bias,  dnn.set_output(), h_size))
        error = compute_cost_function(output[i], output_target)
        dE = bp.compute_dE(output[i], output_target)
        wd = bp.count_weight_decay(weights,wdp)
        mse = bp.overall_cost_function(error, wd)
        dH = bp.compute_dH(weights,  dE, hidden)
        dW = bp.compute_weight_derivative(dH, _dW, dE ,input[i], hidden, wd)
        dB = bp.compute_bias_derivative(dH, _dB, dE, hidden)
        weights = bp.update_weight(dW, weights, learning_rate)
        bias = bp.update_bias(dB, bias, learning_rate)
        end = time.time()

        #"""
        print ("----------------------------------------------------------------------------")
        print (("Informasi Data ke %d ... ... ...") % (i))
        print (("Name \t\t\t : %s") % (path[i]))
        print (("Mean Square error \t : %s ") % (mse) )
        print (("Computation time  \t : %f minute ") % ((end - start)/60))
        print ("----------------------------------------------------------------------------")



if __name__ == '__main__':
    main()
