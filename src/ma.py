import autoencoder as ae
from build_network import NeuralNetworkArch
import pandas as pd
import numpy as np
import math
import os
import time


def load_data(file):
    f = open(file,"r", encoding="utf8")
    data = f.readlines()
    f.close()
    return data

def copy_data_matrix(data):

    data_txt = data
    data_mtx = []

    for line in data_txt:
        data_col = line.split('\n')
        tmp_value = data_col[0]
        data_mtx.append(float(tmp_value))

    return data_mtx

def load_all_data(file):

    path = []
    f = open(file, "r", encoding="utf8")
    dataset = f.readlines()
    for i in dataset:
        data_col = i.split('\n')
        path.append(data_col[0])
    f.close()

    all_path = []
    for line in range(0,len(path)):
        for root, dirs, files in os.walk(path[line]):
            for filename in files:
                if filename[0:5] == "_NORM" :
                    all_path.append(path[line] + filename)
    return all_path

def encoder_forward_pass(input, weights, hidden_layer, bias,  h_size):

    for l in range(0, len(h_size)  ):
        if l == 0 :
            hidden_layer[l] = ae.create_network(weights[l], input, bias[l], h_size[l], len(input))
        else :
            hidden_layer[l] = ae.create_network(weights[l], hidden_layer[l-1], bias[l], h_size[l], len(hidden_layer[l-1]))
    return hidden_layer

def decoder_forward_pass(input, weights, hidden_layer, bias,  h_size):
    for l in range(0, len(h_size)  ):
        if l == 0 :
            hidden_layer[l] = ae.create_network(weights[l], input, bias[l + len(h_size)], h_size[l+1], len(input))
        elif l != len(h_size) -1 :
            hidden_layer[l] = ae.create_network(weights[l], hidden_layer[l-1], bias[l + len(h_size)], h_size[l+1], len(hidden_layer[l-1]))
    return hidden_layer

def recontruction_input(input, weights,  bias,  output, h_size):

    for l in range(0, len(h_size)  ):
        if l == len(h_size) -1 :
            output = ae.create_network(weights[l], input, bias[l+len(h_size)], len(output), len(input))
    return output

def compute_cost_function(output, y_target):
    sum = 0
    error_cf = []
    for i in range(0,len(output)):
         error_cf.append(ae.cost_function(output[i], y_target[i]))

    return error_cf

def main():

    """
    :wdp = weight decay parameter
    """
    x_size = 54675
    h_size = [500, 400, 300, 400, 500]
    encoder_size = [500, 400, 300]
    decoder_size = [300, 400, 500]
    hl_total = 3
    y_size = x_size
    wdp = 0.0001
    learning_rate = 0.01
    s_param = 0.01
    control_sparm = 0.001

    print ("Build Network .....")
    dnn = NeuralNetworkArch(x_size, h_size, hl_total, y_size, encoder_size, decoder_size)
    weights = dnn.set_weight()
    output_target = []
    bias = dnn.set_bias()

    print ("Forward Pass ......")

    # registrasi variable
    input = []
    hidden_layer = []
    hidden_encoder = []
    hidden_decoder = []
    recontruction_x = []
    sparsity = []

    # Load Semua file disini
    path = load_all_data("katalog_dataset.txt")


    for i in range(0,len(path)):


        start = time.time()
        h_enc = dnn.set_ench()
        h_dec = dnn.set_dech()

        _dW = dnn.set_derivative_weight()
        _dB = dnn.set_derivatives_bias()

        print ("----------------------------------------------------------------------------")
        print (" Get dataset for input feature ... ...")
        input.append(copy_data_matrix(load_data(path[i])))

        print ("  Compute Forward Pass ... ...")
        print ("\t Compute Encoder ... ...")
        hidden_encoder.append(encoder_forward_pass(input[i], weights, h_enc  , bias,   encoder_size))
        h_enc = hidden_encoder[i]

        print ("\t Transpose Weight ... ...")
        Wt = ae.weights_transpose(weights) # Weight Transpose

        print ("\t Compute Decoder ... ...")
        hidden_decoder.append(decoder_forward_pass(h_enc[len(h_enc)-1], Wt, h_dec  , bias,   decoder_size))
        h_dec = hidden_decoder[i]

        print ("\t Recontruction Input ... ...")
        recontruction_x.append(recontruction_input(h_dec[len(h_dec) - 1 ], Wt,  bias,  dnn.set_output(), decoder_size))

        print ("\t Compute cost function ... ...")
        error = compute_cost_function(recontruction_x[i], input[i] )

        print ("\t Compute weight Decay parameter ... ...")
        wd = ae.count_weight_decay(weights,wdp)

        print ("\t Compute sparsity contrains ... ...")
        sparsity.append(ae.sparsity_constrain( recontruction_x[i], h_dec, h_enc, len(h_size) ))

        print ("\t Compute KullbackLeibler ... ...")
        KL = ae.KullbackLeibler(sparsity[i], s_param)

        print ("\t Compute Overal cost function ... ...")
        mse = ae.overall_cost_function(error, wd, control_sparm, KL)

        print ("  Compute Backpropagation ... ...")

        print ("\t Compute cost function derivatives ... ...")
        dE = ae.compute_dE(recontruction_x[i], input[i])

        print ("\t Compute compute decoder hidden layer derivative ... ...")
        dH_dec = ae.compute_dH(Wt, dE, h_dec, s_param, sparsity[i], control_sparm )

        print ("\t Compute compute encoder hidden layer derivative ... ...")
        dH_enc = ae.compute_encdH(weights, Wt ,dH_dec[len(dH_dec) - 1], h_enc, s_param, sparsity[i], control_sparm )

        print ("\t Compute compute weight derivative ... ...")
        dW = ae.compute_weight_derivative(dH_enc, _dW, dH_dec[len(dH_dec) - 1] ,input[i], h_enc, wdp, weights)

        print ("\t Compute compute bias derivative ... ...")
        dB = ae.compute_bias_derivative(dH_enc, dH_dec, _dB, dE)

        print ("\t Compute Update weights ... ...")
        weights_update = ae.update_weight(dW, weights, learning_rate)

        print ("\t Compute Update bias ... ...")
        bias = ae.update_bias(dB, bias, learning_rate)


        end = time.time()

        print ("----------------------------------------------------------------------------")
        print (("Informasi Data ke %d ... ... ...") % (i))
        print (("Name \t\t\t : %s") % (path[i]))
        print (("Mean Square error \t : %s ") % (mse) )
        print (("Computation time  \t : %f minute ") % ((end - start)/60))
        print (("Weight example 0   \t : %g")%(weights_update[0][0][0]))
        print (("Weight example 0   \t : %g")%(weights[0][0][0]))
        print (("Weight example 1   \t : %g")%(weights_update[1][1][1]))
        print (("Weight example 1   \t : %g")%(weights[1][1][1]))
        print ("============================================================================")

        weights = weights_update


if __name__ == '__main__':
    main()
