import pandas as pd
import numpy as np
import math
import os

def normal_function(data, min, max, new_min, new_max):
    """
    Fungsi untuk menghitung normalisasi data
    parameter --v
    :data merupakan nilai dari data
    :min merupakan nilai terkecil dari data
    :max merupakan nilai terbesar dari data
    :new_max merupakan range terbesar untuk data yang akan dinormalkan
    :new_min rage terkecil dari data yang akan di normalkan
    return data baru dari hasil normalisasi
    """
    return ((data - min )*(new_max - new_min)) / ((max-min) + new_min )

def load_data(file):
    f = open(file,"r", encoding="utf8")
    data = f.readlines()
    f.close()
    return data

def copy_data_matrix(file):
    """
    method untuk copy data dari file ke matrix
    parameter --v
    :data_txt = data file
    :data_mtx = data salinan dari file ke matrix
    :data_col = data per kolom di setial line
    :line = line dalam data_txt
    :tmp_info = mengambil informasi yang ditulis dari file
    :tmp_gene = gene expression value
    return data_mtx
    """

    data_txt = load_data(file)
    data_mtx = []

    for line in data_txt:
        data_col = line.split('\t')
        tmp_info = data_col[0]
        if tmp_info[0] != "#" and tmp_info[0] != "!" and tmp_info[0] != "^"  and tmp_info != "ID_REF":
            tmp_gene = data_col[1]
            data_mtx.append(float(tmp_gene))

    return data_mtx

def find_max(data_mtx):
    ndata = len(data_mtx)
    print (ndata)
    max = 0.0
    for i in range(0,ndata):
        if max < data_mtx[i] :
            max = data_mtx[i]
    return max

def find_min(data_mtx):
    ndata = len(data_mtx)
    min = 9999.0
    for i in range(0,ndata):
        if min > data_mtx[i] :
            min = data_mtx[i]
    return min

def compute_norm(data, new_max, new_min, max, min):
    data_norm = []
    ndata = len(data)
    for i in range(0,ndata):
        tmp_result = normal_function(data[i], min, max, new_min, new_max)
        data_norm.append(tmp_result)
    return data_norm

def write_file(data_norm, file, path):

    filename = "_NORM_" + file
    full_path = path + filename
    ndata = len(data_norm)
    f = open(full_path, "w")
    for i in range(0,ndata):
        f.write(str(data_norm[i]) + '\n')
    f.close()

def main(file, path):

    full_path = path + file

    new_max = 1
    new_min = 0
    data_mtx = copy_data_matrix(full_path)

    max = find_max(data_mtx)
    min = find_min(data_mtx)

    print ("................................................................")
    print ("Informasi Data ...")
    print (("Name \t\t\t : %s") % (file))
    print (("Path \t\t\t : %s") % (full_path))
    print (("Max Data \t\t : %f") % (max))
    print (("Min Data \t\t : %f") % (min))
    print (("Max Normalisasi \t : %f") % (new_max))
    print (("Min Normalisasi \t : %f") % (new_min))
    print (("size GEV \t\t : %d") % (len(data_mtx)))

    data_norm = compute_norm(data_mtx, new_max, new_min, max, min)
    write_file(data_norm, file, path)

    # SIMPAN DATA KE SINI SEMUANYA YAH


if __name__ == '__main__':


    path = []
    f = open("katalog_dataset.txt", "r", encoding="utf8")
    dataset = f.readlines()
    for i in dataset:
        data_col = i.split('\n')
        path.append(data_col[0])
    f.close()



    for line in range(0,len(path)):
        for root, dirs, files in os.walk(path[line]):
            for filename in files:
                main(filename, path[line])
